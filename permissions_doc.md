[#barker_hacking]()

# ROOTADMIN
*

# ADMIN
* access /api/admin APIs
* set site theme
* add user to site
* edit site users information
	1. change email
	1. change display name

* able to view site statistics

# MODERATOR
*

# STAFF
* choose the dog winner
* edit community roles
* edit members of a community

# CREATOR
* publish tips

# COMMUNITY ADMIN

# COMMUNITY MODERATOR

# COMMUNITY USER

# PREMIUM USER
* view "Likes" section

# LOW USER
